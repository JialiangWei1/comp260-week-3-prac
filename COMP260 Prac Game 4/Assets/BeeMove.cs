﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove: MonoBehaviour {

    public float speed = 4.0f;
    public Transform target1;
    public Transform target2;

    void Update()
    {
        // get the vector from the bee to the target
        // and normalise it.
        Transform target;
        target = target1;
        if(Vector2.Distance(target1.position,transform.position)< Vector2.Distance(target2.position, transform.position)){ 
            target = target1;
        }
        else
        {
            target = target2;
        }


        Vector2 direction = target.position - transform.position;
        direction = direction.normalized;
        Vector2 velocity = direction * speed;
        transform.Translate(velocity * Time.deltaTime);
    }
}
