﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveW2 : MonoBehaviour {
    public string horizontalAxis;
    public string verticalAxis;
    public float maxSpeed = 5.0f;
    void Update()
    {
        // get the input values
        Vector2 direction;
        direction.x = Input.GetAxis(horizontalAxis);
        direction.y = Input.GetAxis(verticalAxis);
        // scale by the maxSpeed parameter
        Vector2 velocity = direction * maxSpeed;
        // move the object
        transform.Translate(velocity * Time.deltaTime);
    }
}
