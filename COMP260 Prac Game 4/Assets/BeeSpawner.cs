﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {
    public int nBees = 50;
    public BeeMove1 beePrefab;
    public Transform player1;
    public Transform player2;
    public Rect spawnRect;
    public int BeePeriod;
    private int BeeCount;
    private int deltaTime = 0;
    public int minimum;
    public int maximum;
    private int inter;
    public ParticleSystem explosionPrefab;
    void Start()
    {

    }

    // Update is called once per frame
    void Update () {
        inter = 5;
        deltaTime++;
        if (deltaTime % (inter*60) == 0 && BeeCount<nBees)
        {
            initBee();
            deltaTime = 0;
            inter = Random.Range(minimum, maximum);
            Debug.Log(inter * 50);
        }
		
	}
    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMin));
    }

    public void DestroyBees(Vector2 centre, float radius)
    {
        // destroy all bees within ‘radius’ of ‘centre’
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            // Fixed bug by adding a type conversion
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
                BeeCount--;
            }
        }
    }
    void initBee()
    {
        Debug.Log("initBee"+BeeCount);
        // instantiate a bee
        BeeMove1 bee = Instantiate(beePrefab);
        // attach to this object in the hierarchy
        bee.transform.parent = transform;
        // give the bee a name and number
        bee.gameObject.name = "Bee " + BeeCount;
        BeeCount++;
        //set bee target
        bee.Player1 = player1;
        bee.Player2 = player2;
        // move the bee to a random position within
        // the spawn rectangle
        float x = spawnRect.xMin +
        Random.value * spawnRect.width;
        float y = spawnRect.yMin +
        Random.value * spawnRect.height;
        bee.transform.position = new Vector2(x, y);
        bee.explosionPrefab = this.explosionPrefab;
    }
}
